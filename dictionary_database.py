emp_data = {
    100: {
            'name': 'ramesh',
            'salary': 1000
          },
    101: {
            'name': 'suresh',
            'salary': 2000
          }
}

def print_emp():
    # for emp_id in emp_data:
    #     print('Emp ID : %s, Emp Name : %s, Emp Sal : %s ' %(emp_id, emp_data[emp_id]['name'], emp_data[emp_id]['salary']) )
    print('emp_data :', emp_data)

def add_emp(nemp_id, nemp_name, nemp_sal):

    emp_data[nemp_id] = {'name': nemp_name, 'salary': nemp_sal}
    if nemp_id in emp_data:
        print('Emp %s added successfully' %nemp_id)

def del_emp(d_emp_id):
    if d_emp_id in emp_data:
        del emp_data[d_emp_id]
        if d_emp_id not in emp_data:
            print('Emp %s deleted successfully' %d_emp_id)
    else :
        print('Emp id %s not found' % d_emp_id)


def search_emp(s_emp_id):
    if s_emp_id in emp_data:
        print('Emp id %s found' % s_emp_id)
    else:
        print('Emp id %s not found' % s_emp_id)

if __name__ == '__main__':

    while True:
        print()
        print("01 - Print Employee Data")
        print("02 - Add Employee")
        print("03 - Delete Employee")
        print("04 - Search Employee")
        print("05 - Quit")

        option = int(input('Enter your choice: '))

        if option == 1:
            print_emp()
        elif option == 2:
            emp_id, emp_name, emp_sal = input('Enter Emp_ID Name Salary Space separated:').split()
            add_emp(int(emp_id), emp_name, emp_sal)
        elif option == 3:
            emp_id = int(input('Enter the EMP ID to delete :'))
            del_emp(emp_id)
        elif option == 4:
            s_emp_id = int(input('Enter the EMP ID to search :'))
            search_emp(4
                       )
        elif option == 5:
            print('stopping program')
            break
        else:
            print("Wrong Option")
            print()
