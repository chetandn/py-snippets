""" # without re
from zipfile import ZipFile

not_ok_str_list = [b'fail', b'false']

with ZipFile(r'C:\temp\temp.zip') as myzip:
    for ele in myzip.namelist():
        with myzip.open(ele, mode='U') as myfile:
            for line in myfile:
                if any(s in line.lower() for s in not_ok_str_list):  # make case insensitive .lower()
                    print(line.decode().strip())
                    
"""
from zipfile import ZipFile
import re

not_ok_str_list = [b'fail', b'false']  

with ZipFile(r'C:\temp\temp.zip') as myzip:
    for ele in myzip.namelist():
        with myzip.open(ele, mode='U') as myfile:
            for line in myfile:
                if any(re.search(not_ok_str, line, re.IGNORECASE) for not_ok_str in not_ok_str_list):                    
                    print(line.decode().strip())