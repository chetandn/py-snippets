import os
import xml.etree.ElementTree as ET

in_file = os.path.join(os.path.dirname(os.getcwd()), 'data', 'in_file.xml')


tree = ET.parse(in_file)
root = tree.getroot()

print('root.tag :', root.tag)

for project in root.findall('Project'):
    path_ele = project.find('Path').text

    if path_ele != 'Config01.uprj':
        print('path_ele :', path_ele)
        root.remove(project)


project = ET.SubElement(root, 'Project')

project.set('Ver', '2.6')
name = ET.SubElement(project, 'Name')
name.text = 'Config04'


tree.write('output.xml', xml_declaration=True, encoding="utf-8")


