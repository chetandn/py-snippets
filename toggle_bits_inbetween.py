'''
Question - Given a number, set MSB, Clear LSB and toggle all bits in between
'''


import math

def toggle_inbetween(num):

    msb = int(math.log(num, 2))
    lsb = int(math.log(num & - num, 2))

    print('msb :', msb)
    print('lsb :', lsb)
    
    # since we need to reset lsb, go toggling from lsb till (msb-1) and retain msb
    for i in range(lsb, msb):
        num = num ^ 1 << i

    return num


a = 0b00101100
print('Before a :', bin(a))
a = toggle_inbetween(a)
print('After a :', bin(a))